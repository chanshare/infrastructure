.PHONY: stubs

all: 
	docker compose up -d

provisioner:
	docker compose up -d wiremock prov-db jaeger

manager:
	docker compose up -d nats jaeger wiremock

stubs: 
	./stubs/content-service.sh
